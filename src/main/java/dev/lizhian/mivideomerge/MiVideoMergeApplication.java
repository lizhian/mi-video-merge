package dev.lizhian.mivideomerge;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.RuntimeUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.system.OsInfo;
import cn.hutool.system.SystemUtil;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootApplication
public class MiVideoMergeApplication {

    public static final PrintStream log = System.out;

    public static void main(String[] args) {
        String rootPath = getRootPath();
        String ffmpeg = getFFmpeg(rootPath);
        log.println("当前目录:" + rootPath);
        log.println("ffmpeg目录:" + ffmpeg);
        exec(true, ffmpeg);
        String inputPath = rootPath + File.separator + "输入";
        List<File> videoPaths = getVideoPaths(inputPath);
        log.println("扫描【" + inputPath + "】共有文件夹:" + videoPaths.size());
        videoPaths.forEach(file -> log.println(file.getPath()));
        log.println();
        for (File videoPath : videoPaths) {
            List<String> videoItems = getVideoItems(videoPath)
                    .stream()
                    .map(file -> "file '" + file.getPath() + "'")
                    .collect(Collectors.toList());

            if (videoItems.size() < 1) {
                continue;
            }
            String tempFile = inputPath + File.separator + "tempFile.txt";
            FileUtil.writeUtf8Lines(videoItems, tempFile);
            log.println("目录【" + videoPath + "】共有视频文件:" + videoItems.size() + ",开始个合并...");
            String outputFile = rootPath + File.separator + "输出" + File.separator + videoPath.getName() + ".mp4";
            exec(true
                    , ffmpeg
                    , "-y"
                    , "-f"
                    , "concat"
                    , "-safe"
                    , "0"
                    , "-i"
                    , tempFile
                    , "-c"
                    , "copy"
                    , "-strict"
                    , "-2"
                    , outputFile
            );
            log.println("合并成功,输出文件【" + outputFile + "】");
            log.println();
        }
        //SpringApplication.run(MiVideoMergeApplication.class, args);
    }

    private static List<File> getVideoItems(File videoPath) {
        return Arrays.stream(FileUtil.ls(videoPath.getAbsolutePath()))
                .filter(item -> NumberUtil.isInteger(item.getName()))
                .sorted(Comparator.comparing(item -> Integer.valueOf(item.getName())))
                .collect(Collectors.toList());
    }

    private static List<File> getVideoPaths(String inputPath) {
        File[] files = FileUtil.ls(inputPath);
        return Arrays.stream(files)
                .filter(File::isDirectory)
                .collect(Collectors.toList());
    }

    private static void exec(boolean println, String... cmds) {
        try {
            String exec = RuntimeUtil.execForStr(cmds);
            if (println) {
                log.println(exec);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getFFmpeg(String rootPath) {
        OsInfo osInfo = SystemUtil.getOsInfo();
        if (osInfo.isWindows()) {
            return StrUtil.join(File.separator
                    , rootPath
                    , "ffmpeg-for-win"
                    , "bin"
                    , "ffmpeg.exe"
            );
        }
//        return "ffmpeg";
        return StrUtil.join(File.separator
                , rootPath
                , "ffmpeg-for-mac"
                , "bin"
                , "ffmpeg"
        );
    }

    private static String getRootPath() {
        File webRoot = FileUtil.getWebRoot();
        if (webRoot.getAbsolutePath().endsWith(FileUtil.JAR_PATH_EXT)) {
            return webRoot.getParent();
        } else {
            return webRoot.getAbsolutePath();
        }
    }
}